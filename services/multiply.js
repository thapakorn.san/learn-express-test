const multiplyFloat = require('../managers/multiplyFloat');
const multiplyMinus = require('../managers/multiplyMinus');
const multiplyInteger = require('../managers/multiplyInteger');
const { isFloat, isInt, isMinus, isNumber} = require('../utils/checkNumber')

const multiplyFlow = async (a, b) => {
    if (!isNumber(a) || !isNumber(b)) {
        throw new Error("Parameters are not correct");
    }

    if (isMinus(a) || isMinus(b)) {
        return multiplyMinus(a, b);
    }
    else if (isFloat(a) || isFloat(b)) {
        return multiplyFloat(a, b);
    }
    else if (isInt(a) || isInt(b)) {
        return multiplyInteger(a, b);
    } else {
        throw new Error("Not catch by any block");
    }
}

module.exports = multiplyFlow;