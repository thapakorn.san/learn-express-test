
function checkNumber(n){
    return Number(n) === n && n % 1 === 0;
}

function isFloat(n){
    return Number(n) === n && n % 1 !== 0;
}

function isMinus(n) {
    return n < 0;
}

function isNumber(n) {
    return !isNaN(n);
}

module.exports = {
    isInt: checkNumber,
    isFloat,
    isMinus,
    isNumber
}