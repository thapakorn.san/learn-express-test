var express = require('express');
const multiplyFlow = require('../services/multiply');
var router = express.Router();


/* GET home page. */
router.get('/', function(req, res, next) {
  res.send("It is working!!!");
  res.end();
});

router.post('/multiply', async (req, res) => {
  try {
    const {a , b} = req.body;
    const multiplyResult = await multiplyFlow(Number(a), Number(b));
    res.status(200).send(`Result: ${multiplyResult}`);
  } catch (e) {
    res.status(400).send(e.message);
    res.end();
  }
});

module.exports = router;
