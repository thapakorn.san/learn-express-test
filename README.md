### Description
This is the project for learning how to write test with express and jest

## The purpose of this task 
Tester can write test for API behavior whether is can work correctly or not.

# Testing case
1. Test service can call manager correctly by each case
2. Call API can handle wrong parameter correctly if the input is not number.

# Cautions
Please remember to not fix anything in managers and all function in managers are simulate the the request to external API which need to call with promise