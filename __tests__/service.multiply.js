describe("Simple test", () => {
    test("It should plus correctly", () => {
        expect(1 + 1).toBe(2);
    });

    test("It should multiply correctly", () => {
        expect(2*2).toBe(4);
    })
});